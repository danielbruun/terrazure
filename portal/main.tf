# Configure the Azure provider
# Extensions Hashicorp terraform & Azure Terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.41.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "resourcegroup01" {
  name     = var.resource_group_name
  location = var.location
}

#Create KeyVault ID
resource "random_id" "kvname" {
  byte_length = 5
  prefix = "keyvault"
}

#Keyvault Creation
data "azurerm_client_config" "current" {}
resource "azurerm_key_vault" "portalvault" {
  depends_on = [ azurerm_resource_group.resourcegroup01 ]
  name                        = var.portalvault
  location                    = var.location
  resource_group_name         = var.resource_group_name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "get",
    ]

    secret_permissions = [
      "get", "backup", "delete", "list", "purge", "recover", "restore", "set",
    ]

    storage_permissions = [
      "get",
    ]
  }
}

#Create Key Vault Secret
resource "azurerm_key_vault_secret" "vmpassword" {
  name         = "vmpassword"
  value        = var.portalpw
  key_vault_id = azurerm_key_vault.portalvault.id
  depends_on = [ azurerm_key_vault.portalvault ]
}

resource "azurerm_virtual_network" "azvnet" {
  name                = "${var.resource_group_name}-vnet"
  location            = var.location
  address_space       = ["10.0.0.0/16"]
  resource_group_name = azurerm_resource_group.resourcegroup01.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.resource_group_name}-snet"
  address_prefixes       = ["10.0.1.0/24"]
  resource_group_name  = azurerm_resource_group.resourcegroup01.name
  virtual_network_name = azurerm_virtual_network.azvnet.name
}

resource "azurerm_public_ip" "static" {
  name                = "${var.resource_group_name}-vm-pip"
  location            = var.location
  resource_group_name = azurerm_resource_group.resourcegroup01.name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "vm_nic" {
  name                = "${var.resource_group_name}-vm-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.resourcegroup01.name
  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.static.id
  }
}

resource "azurerm_windows_virtual_machine" "simple-vm" {
  name                = var.vm_name
  resource_group_name = azurerm_resource_group.resourcegroup01.name
  location            = var.location
  size                = "Standard_D2s_v3"
  admin_username      = var.portaluser
  admin_password      = var.portalpw

  network_interface_ids = [
    azurerm_network_interface.vm_nic.id,
  ]
  os_disk {
    name                 = join("_", [var.vm_name, "OsDisk"])
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
}

output "vm-name" {
  value = azurerm_windows_virtual_machine.simple-vm.name
}

output "public-ip" {
  value = azurerm_public_ip.static.ip_address
}