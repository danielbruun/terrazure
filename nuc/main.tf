# Configure the Azure provider
# Extensions Hashicorp terraform & Azure Terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.41.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "resourcegroup_nuc" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_virtual_network" "azvnet_nuc" {
  name                = "${var.resource_group_name}-vnet"
  location            = var.location
  address_space       = ["10.0.0.0/16"]
  resource_group_name = azurerm_resource_group.resourcegroup_nuc.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.resource_group_name}-snet"
  address_prefixes       = ["10.0.1.0/24"]
  resource_group_name  = azurerm_resource_group.resourcegroup_nuc.name
  virtual_network_name = azurerm_virtual_network.azvnet_nuc.name
}

resource "azurerm_public_ip" "static" {
  name                = "${var.resource_group_name}-vm-pip"
  location            = var.location
  resource_group_name = azurerm_resource_group.resourcegroup_nuc.name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "vm_nic" {
  name                = "${var.resource_group_name}-vm-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.resourcegroup_nuc.name
  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.static.id
  }
}

resource "azurerm_windows_virtual_machine" "nucbro" {
  name                = var.vm_name
  resource_group_name = azurerm_resource_group.resourcegroup_nuc.name
  location            = var.location
  size                = "Standard_D2s_v3"
  admin_username      = var.user
  admin_password      = var.portalpw

  network_interface_ids = [
    azurerm_network_interface.vm_nic.id,
  ]
  os_disk {
    name                 = join("_", [var.vm_name, "OsDisk"])
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "shutdown_nuc" {
 virtual_machine_id = azurerm_windows_virtual_machine.nucbro.id
 location           = azurerm_resource_group.resourcegroup_nuc.location
 enabled            = true

 daily_recurrence_time = "0100"
 timezone              = "W. Europe Standard Time"

 notification_settings {
   enabled         = false
   time_in_minutes = "30"
   webhook_url     = "https://sample-webhook-url.example.com"
 }
}

output "vm-name" {
  value = azurerm_windows_virtual_machine.nucbro.name
}

output "public-ip" {
  value = azurerm_public_ip.static.ip_address
}